import torch
from torch import nn

from data import make_data
from util import score_ap, fit_score

# simple feed-forward multilabel classifier
def make_ml_clf(n_features, n_hid, n_labels, dp, n_layers=2):
    layers = [
        nn.Linear(n_features, n_hid),
        nn.Dropout(dp),
        nn.ReLU()]

    for _ in range(1, n_layers - 1):
        layers.extend([
            nn.Linear(n_hid, n_hid),
            nn.Dropout(dp),
            nn.ReLU()])

    layers.append(nn.Linear(n_hid, n_labels))
    return nn.Sequential(*layers)


if __name__ == '__main__':

    # data params
    n_train = 10000
    n_valid = 3000
    n_test = 10000
    n_features = 300
    n_labels = 40
    avg_labels = 5

    # fixed model params
    n_hid = 512
    n_layers = 2

    cuda = True
    torch.manual_seed(42)

    train, valid, test = make_data(
        n_train,
        n_valid,
        n_test,
        n_features=n_features,
        n_labels=n_labels,
        avg_labels=avg_labels,
        random_state=0,
        cuda=cuda)

    best_ap = 0
    best_model = None

    for lr_exp in (-5, -4, -3, -2, -1, 0):
        lr = 0.001 * (2 ** lr_exp)
        for dp in (.2, .3, .4, .5):
            ff = make_ml_clf(n_features, n_hid, n_labels, dp, n_layers=2)
            if cuda:
                ff = ff.cuda()

            ap = fit_score(ff, train, valid, lr=lr)
            if ap > best_ap:
                print(f"best: lr={lr} drop={dp} AP={ap}")
                best_ap, best_model = ap, ff

    # compute average precision on test set
    test_ap = score_ap(best_model, *test.tensors)
    print("Test AP", ap)

