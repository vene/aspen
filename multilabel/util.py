import numpy as np

import torch
from torch.utils import data as tdata

from sklearn.metrics import average_precision_score


def score_ap(model, X, Y):
    Yhat = model(X)
    Yhat = Yhat.cpu().detach().numpy()
    Y = Y.detach().numpy()
    return average_precision_score(Y, Yhat)


def fit_score(model, train, valid, lr, batch_size=128, max_iter=50, **kwargs):

    X_val, Y_val = valid.tensors

    train_iter = tdata.DataLoader(train, shuffle=True, batch_size=batch_size)
    optim = torch.optim.Adam(model.parameters(), lr=lr, amsgrad=True)

    for t in range(max_iter):

        for X, Y in train_iter:
            optim.zero_grad()
            model.train()
            Yhat = model(X)
            loss = torch.nn.functional.multilabel_soft_margin_loss(Yhat, Y)
            loss.backward()
            optim.step()

    model.eval()
    return score_ap(model, X_val, Y_val)
