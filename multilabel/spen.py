import torch
from torch import nn

from data import make_data
from util import score_ap, fit_score


class EnergyNetwork(nn.Module):
    """ Energy network (SPEN) with unrolled gradient descent"""
    def __init__(self, n_features, n_labels, n_hid, dp, lr=1.0, n_iter=10):
        super().__init__()
        self.n_labels = n_labels

        self.enc = nn.Sequential(
                nn.Linear(n_features, n_hid),
                nn.Dropout(dp),
                nn.ReLU())

        self.initial_guess = nn.Linear(n_hid, n_labels)

        # energy model
        self.W_energy = nn.Linear(n_hid + n_labels, n_hid, bias=True)
        self.v_energy = nn.Linear(n_hid, 1, bias=True)

        self.lr = lr
        self.n_iter = n_iter

    def ml_energy(self, y, h):
        """
        Energy of a configuration given inputs
        E(y, h(x); theta)

        y is a vector, shape = (batch_size, n_labels)
        h is the encoded input, shape = (batch_size, n_hid)

        This can be any differentiable function. PyTorch handles
        the gradient computation. Here, we use a feed-forward net with
        a single tanh layer, applied to the concatenation [y; h].
        """

        hid = torch.cat((y, h), dim=-1)
        hid = torch.tanh(self.W_energy(hid))
        erg = self.v_energy(hid)
        return erg

    def link(self, logits):
        """
        Link function

        Maps logits in R^n to cross-product of simplices (problem specific.)
        For multi-label, this is sigmoid 1/1+exp(-x). (ie, binary variables.)
        """
        return torch.sigmoid(logits)

    def forward(self, x, apply_link=False):
        lr = self.lr
        n_iter = self.n_iter

        # initialize prediction
        h = self.enc(x)
        logits = self.initial_guess(h)

        for _ in range(n_iter):

            # make a new independent sub-computation graph for this iteration
            logits_ = logits.clone().detach().requires_grad_()
            y_ = self.link(logits_)
            energy = self.ml_energy(y_, h).sum()

            # evaluate the gradient \nabla_logits E(y, h(x))
            # since create_graph=True, g is itself auto-differentiable!
            g, = torch.autograd.grad(energy, logits_, create_graph=True)

            # gradient update to logits
            logits = logits - lr * g

        if apply_link:
            return self.link(logits)
        else:
            return logits


if __name__ == '__main__':

    # data params
    n_train = 10000
    n_valid = 3000
    n_test = 10000
    n_features = 300
    n_labels = 40
    avg_labels = 5

    # fixed model params
    n_hid = 512

    cuda = True
    torch.manual_seed(42)

    train, valid, test = make_data(
        n_train,
        n_valid,
        n_test,
        n_features=n_features,
        n_labels=n_labels,
        avg_labels=avg_labels,
        random_state=0,
        cuda=cuda)

    # simple grid search
    best_ap = 0
    best_model = None

    for lr_exp in (-5, -4, -3, -2, -1, 0):
        lr = 0.001 * (2 ** lr_exp)
        for dp in (.2, .3, .4, .5):
            model = EnergyNetwork(n_features, n_labels, n_hid,
                                  dp=dp, n_iter=10, lr=1)
            if cuda:
                model = model.cuda()

            ap = fit_score(model, train, valid, lr=lr)
            if ap > best_ap:
                print(f"best: lr={lr} drop={dp} AP={ap}")
                best_ap, best_model = ap, model

    # compute average precision on test set
    test_ap = score_ap(best_model, *test.tensors)
    print("Test AP", ap)

