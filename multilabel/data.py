import numpy as np
import torch
from torch.utils import data as tdata

from sklearn.datasets import make_multilabel_classification
from sklearn.utils import check_random_state


def make_data(
        n_train,
        n_valid,
        n_test,
        n_features=300,
        n_labels=5,
        avg_labels=2,
        cuda=False,
        random_state=None):

    n_samples = n_train + n_valid + n_test

    rng = check_random_state(random_state)
    X, Y = make_multilabel_classification(
            n_samples,
            n_features,
            n_classes=n_labels,
            n_labels=avg_labels,
            random_state=random_state)

    ix = np.arange(n_samples)
    train_ix, ix = ix[:n_train], ix[n_train:]
    valid_ix, test_ix = ix[:n_valid], ix[n_valid:]

    assert len(np.intersect1d(train_ix, valid_ix)) == 0
    assert len(np.intersect1d(train_ix, test_ix)) == 0
    assert len(np.intersect1d(valid_ix, test_ix)) == 0

    X = torch.from_numpy(X).to(torch.float)
    Y = torch.from_numpy(Y).to(torch.float)

    X_train, Y_train = X[train_ix], Y[train_ix]
    X_valid, Y_valid = X[valid_ix], Y[valid_ix]
    X_test, Y_test = X[test_ix], Y[test_ix]

    if cuda:
        X_train = X_train.cuda()
        Y_train = Y_train.cuda()
        X_valid = X_valid.cuda()
        X_test = X_test.cuda()
        # we keep Y_valid and Y_test on CPU for computing scores

    train = tdata.TensorDataset(X_train, Y_train)
    valid = tdata.TensorDataset(X_valid, Y_valid)
    test = tdata.TensorDataset(X_test, Y_test)

    return train, valid, test
