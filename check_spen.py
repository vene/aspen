import numpy as np
import torch
from torch import nn

from numdifftools import Gradient


if __name__ == '__main__':

    torch.manual_seed(42)
    torch.set_default_tensor_type('torch.DoubleTensor')

    n_features = 4
    n_labels = 3
    n_hid = 5

    initial_guess = nn.Linear(n_features, n_labels)
    W = nn.Linear(n_features + n_labels, n_hid, bias=True)
    v = nn.Linear(n_hid, 1, bias=True)

    def energy(x, y):
        p = torch.cat((y, x), dim=1)
        return v(torch.tanh(W(p)))

    x = torch.randn(1, n_features)

    def np_energy(y_):
        y_ = torch.from_numpy(y_).unsqueeze(0)
        return energy(x, y_).item()

    G = Gradient(np_energy)

    def tG(y):
        y = y.clone().requires_grad_()

        shape = list(x.shape)
        shape[0] = y.shape[0]
        x_ = x.expand(shape)

        E = energy(x_, y).sum()
        E.backward(create_graph=True)
        return y.grad

    def taG(y):
        y = y.clone().requires_grad_()

        shape = list(x.shape)
        shape[0] = y.shape[0]
        x_ = x.expand(shape)

        E = energy(x_, y).sum()
        grad, = torch.autograd.grad(E, y, only_inputs=True, create_graph=True)
        return grad


    y1 = torch.sigmoid(torch.randn(1, n_labels))
    print(G(y1.numpy().squeeze()))
    print(tG(y1).detach().numpy())
    print(taG(y1))

    y2 = torch.sigmoid(torch.randn(1, n_labels))
    print(G(y2.numpy().squeeze()))
    print(tG(y2).detach().numpy())
    print(taG(y2))

    Y = torch.cat((y1, y2))
    print(tG(Y).detach().numpy())
    print(taG(Y))









